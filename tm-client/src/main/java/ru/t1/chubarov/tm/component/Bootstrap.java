package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.repository.ICommandRepository;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chubarov.tm.exception.system.CommandNotSupportedException;
import ru.t1.chubarov.tm.repository.CommandRepository;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.util.SystemUtil;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.chubarov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> claszz : classes) {
            try {
                regestry(claszz);
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    private void regestry(@NotNull final Class<? extends AbstractCommand> clazz) throws IllegalAccessException, InstantiationException {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        regestry(command);
    }

    private void regestry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) throws AbstractException, IOException {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() throws AbstractException {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        fileScanner.start();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                prepareShutdown();
            }
        });
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private boolean processArguments(@Nullable final String[] args) throws AbstractException, IOException {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    private void processArgument(@Nullable final String arg) throws AbstractException, IOException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) throws AbstractException, IOException {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) throws AbstractException, IOException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }


}
