package ru.t1.chubarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.ProjectGetByIndexRequest;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {
    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken());
        request.setIndex(index);
        //getProjectEndpoint().getProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display project by index.";
    }

}
