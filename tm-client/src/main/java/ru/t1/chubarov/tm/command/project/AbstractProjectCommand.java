package ru.t1.chubarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

}
