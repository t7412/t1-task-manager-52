package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IUserEndpoint;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    public IAuthEndpoint getAuthEndPoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    public void showUser(@Nullable final UserDTO user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("  LOGIN: " + user.getLogin());
        System.out.println("  EMAIL: " + user.getEmail());
        System.out.println("  FIRST NAME: " + user.getFirstName());
        System.out.println("  LAST NAME: " + user.getLastName());
        System.out.println("  MIDDLE NAME: " + user.getMiddleName());
    }

}
