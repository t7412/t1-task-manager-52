package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @NotNull final TaskDTO task = getTaskEndpoint().getTaskById(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display task by id.";
    }

}
