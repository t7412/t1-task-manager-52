package ru.t1.chubarov.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);

    @Nullable
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("taskmanager");

    @SneakyThrows
    public void log(@NotNull final String json) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection("taskmanager");
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
        System.out.println(json);
    }

}