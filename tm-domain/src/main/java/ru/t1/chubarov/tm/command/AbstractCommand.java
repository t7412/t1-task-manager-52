package ru.t1.chubarov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.model.ICommand;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;

import java.io.IOException;

public abstract class AbstractCommand implements ICommand {

    public abstract void execute() throws AbstractException, IOException;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

