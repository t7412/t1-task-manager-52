package ru.t1.chubarov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public interface ITaskModelRepository extends IModelRepository<Task> {

    @Nullable
    List<Task> findAllByUser(@Nullable String userId);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task findOneByIdByUser(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    void remove(@Nullable String userId, @NotNull Task model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    long getSizeByUser(@Nullable String userId);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

}
