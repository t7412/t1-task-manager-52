package ru.t1.chubarov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDtoRepository extends IDtoRepository<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll();

    @Nullable
    List<ProjectDTO> findAllByUser(@Nullable String userId);

    @Nullable
    ProjectDTO findOneById(@NotNull String id);

    @Nullable
    ProjectDTO findOneByIdByUser(@Nullable String userId, @Nullable String id);

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, @NotNull ProjectDTO model);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    void clear();

    int getSizeByUser(@Nullable String userId);

    int getSize();

}
