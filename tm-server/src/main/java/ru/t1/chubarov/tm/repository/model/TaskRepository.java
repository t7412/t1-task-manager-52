package ru.t1.chubarov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskModelRepository {


    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        return entityManager
                .createQuery("SELECT p FROM Task p ", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p FROM Task p WHERE p.user.id = :userId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable String userId,@Nullable String projectId) {
        return entityManager
                .createQuery("SELECT p FROM Task p WHERE p.user.id = :userId AND p.project.id = :projectId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String id) {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p FROM Task p WHERE p.user.id = :userId AND p.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull Task model) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM Task p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Task p", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Task p WHERE p.user.id = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) = 1 FROM Task p WHERE p.user.id = :userId and p.id = :id", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getSingleResult();
    }

}
