package ru.t1.chubarov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.ISessionModelRepository;

import ru.t1.chubarov.tm.model.Session;


import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionModelRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        return entityManager
                .createQuery("SELECT p FROM Session p ", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Session> findAllByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT p FROM Session p WHERE p.user.id = :userId", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String id) {
        return entityManager
                .find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneByIdByUser(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT p FROM Session p WHERE p.user.id = :userId AND p.id = :id", Session.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setFirstResult(0)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        entityManager
                .createQuery("DELETE FROM Session p WHERE p.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void remove(@Nullable String userId, @NotNull Session model) {
        entityManager
                .createQuery("DELETE FROM Session p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", model.getId())
                .executeUpdate();
    }

    @Override
    public void removeOneById(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM Session p WHERE p.user.id = :userId and p.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Session p", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public long getSizeByUser(@Nullable String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM Session p WHERE p.user.id = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        return entityManager
                .createQuery("SELECT COUNT(p) = 1 FROM Session p WHERE p.user.id = :userId and p.id = :id", Boolean.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getSingleResult();
    }

}
