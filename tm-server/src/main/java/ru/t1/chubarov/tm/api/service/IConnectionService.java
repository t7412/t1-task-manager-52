package ru.t1.chubarov.tm.api.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.sql.Connection;

public interface IConnectionService {

    @SneakyThrows
    @NotNull
    EntityManager getEntityManager();

}
