package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.IProjectTaskService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;

public interface IServiceLocator {

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IPropertyService getPropertyService();

    @Nullable
    ITaskDtoService getTaskDtoService();

    @Nullable
    IProjectTaskDtoService getProjectTaskDtoService();

    @Nullable
    ILoggerService getLoggerService();

    @Nullable
    abstract IUserDtoService getUserDtoService();

    @Nullable
    IAuthService getAuthService();

    @Nullable
    IDomainService getDomainService();

    @Nullable
    ICommandService getCommandService();

    //
    @NotNull
    IProjectService getProjectService();

    @Nullable
    ITaskService getTaskService();

    @Nullable
    IProjectTaskService getProjectTaskService();

    @Nullable
    abstract IUserService getUserService();

}
