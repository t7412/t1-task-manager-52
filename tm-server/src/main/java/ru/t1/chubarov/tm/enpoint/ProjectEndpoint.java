package ru.t1.chubarov.tm.enpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.chubarov.tm.api.service.IServiceLocator;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

import static ru.t1.chubarov.tm.enumerated.ProjectSort.BY_NAME;

@WebService(endpointInterface = "ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectDtoService getProjectService() {
        return getServiceLocator().getProjectDtoService();
    }

    @NotNull
    private IProjectTaskDtoService getProjectTaskService() {
        return getServiceLocator().getProjectTaskDtoService();
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().clear();
        return new ProjectClearResponse();
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectGetByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectShowResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull ProjectSort sort = request.getSort();
        if (sort == null) sort = BY_NAME;
        @Nullable final List<ProjectDTO> projects = getProjectService().findAll(userId);
        return new ProjectShowResponse(projects);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDTO project = getProjectService().removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectTaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectTaskBindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new ProjectTaskBindToProjectResponse();
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectTaskUnbindToProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectTaskUnbindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new ProjectTaskUnbindToProjectResponse();
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

}
