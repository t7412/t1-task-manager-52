package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.service.model.ProjectService;
import ru.t1.chubarov.tm.service.model.TaskService;
import ru.t1.chubarov.tm.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private String userUserId = "";

    @NotNull
    private String userAdminId = "";


    @Before
    public void initTest() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        IUserService userService = new UserService(projectService, taskService, propertyService, connectionService);
        projectList = new ArrayList<>();
        @NotNull User admin;
        @NotNull User user;
        if (userService.isLoginExist("admin")) {
            admin = userService.findByLogin("admin");
        } else {
            admin = userService.create("admin", "admin", Role.ADMIN);
        }
        if (userService.isLoginExist("user")) {
            user = userService.findByLogin("user");
        } else {
            user = userService.create("user", "user", "user@emal.ru");
        }
        userUserId = user.getId();
        userAdminId = admin.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project Name " + i);
            project.setDescription("description test " + i);
            if (i <= 1) {
                project.setUser(admin);
                projectService.add(project);
            } else {
                project.setUser(user);
                projectService.add(project);
            }
            projectList.add(project);
        }
    }

    @After
    public void finish() {
        projectList.clear();
        projectService.clear();
    }

    @Test
    public void testSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
    }

    @SneakyThrows
    @Test
    public void testCreate() {
        projectService.create(userUserId, "project with desc", "project description");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize());
    }

    @Test
    public void testCreateNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create("", "project_create", "project description"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, "project_create", "project description"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(userUserId, "", "project description"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(userUserId, "project_create", ""));
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        @NotNull final String projectId = projectList.get(0).getId();
        @Nullable final String userId = projectList.get(0).getUser().getId();
        Assert.assertEquals(projectId, projectService.findOneById(userId, projectId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById("", projectId).getId());
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<Project> userProjectList = projectService.findAll(userUserId);
        Assert.assertEquals(2, userProjectList.size());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        projectService.remove(userUserId, projectList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        projectService.removeOneById(userUserId, projectList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectService.getSize());
    }

    @SneakyThrows
    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String projectId = projectList.get(1).getId();
        @NotNull final String userId = projectList.get(1).getUser().getId();
        Assert.assertEquals(Status.NOT_STARTED.toString(), projectService.findOneById(userId, projectId).getStatus());
        projectService.changeProjectStatusById(userUserId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS.toString(), projectService.findOneById(userId, projectId).getStatus());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", projectId, Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, projectId, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userUserId, "", Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userUserId, null, Status.IN_PROGRESS));
    }

    @SneakyThrows
    @Test
    public void testUpdateById() {
        @NotNull final String projectId = projectList.get(1).getId();
        @NotNull final String userId = projectList.get(1).getUser().getId();
        Assert.assertEquals("Project Name 2", projectService.findOneById(userId, projectId).getName());
        Assert.assertEquals("description test 2", projectService.findOneById(userId, projectId).getDescription());
        projectService.updateById(userUserId, projectId, "NewNameProject", "NewDescriptionProject");
        Assert.assertEquals("NewNameProject", projectService.findOneById(userId, projectId).getName());
        Assert.assertEquals("NewDescriptionProject", projectService.findOneById(userId, projectId).getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", projectId, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, projectId, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userUserId, "", "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userUserId, null, "NewNameProject", "NewDescriptionProject"));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userUserId, projectId, "", "NewDescriptionProject"));

    }

}
