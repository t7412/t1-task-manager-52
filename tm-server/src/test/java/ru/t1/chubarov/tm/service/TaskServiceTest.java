package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.api.service.dto.IProjectDtoService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.dto.IUserDtoService;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.service.dto.ProjectDtoService;
import ru.t1.chubarov.tm.service.dto.TaskDtoService;
import ru.t1.chubarov.tm.service.dto.UserDtoService;
import ru.t1.chubarov.tm.service.model.ProjectService;
import ru.t1.chubarov.tm.service.model.TaskService;
import ru.t1.chubarov.tm.service.model.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(projectService, taskService, propertyService, connectionService);

    @NotNull
    private List<Task> taskList;

    @NotNull
    private final String taskProjectId = UUID.randomUUID().toString();

    @NotNull
    private String userUserId = "";

    @NotNull
    private String userAdminId = "";
    @NotNull
    final Project projectTask = new Project();

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    Project project = new Project();

    @Before
    public void initTest() throws Exception {
        taskList = new ArrayList<>();
        if (userService.isLoginExist("admin")) {
            admin = userService.findByLogin("admin");
        } else {
            admin = userService.create("admin", "admin", Role.ADMIN);
        }
        if (userService.isLoginExist("user")) {
            user = userService.findByLogin("user");
        } else {
            user = userService.create("user", "user", "user@emal.ru");
        }
        userUserId = user.getId();
        userAdminId = admin.getId();

        projectTask.setId(taskProjectId);
        projectTask.setName("project for Task");
        projectTask.setDescription("project description for Task.");
        projectTask.setUser(admin);
        projectService.add(projectTask);

        project.setName("Project Name ");
        project.setDescription("description test ");
        project.setId(taskProjectId);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task Name " + i);
            task.setDescription("description test " + i);
            if (i <= 1) {
                task.setUser(admin);
                taskService.add(userAdminId, task);
            } else {
                task.setUser(user);
                if (i == 2) task.setProject(project);
                taskService.add(userUserId, task);
            }
            taskList.add(task);
        }
    }

    @After
    public void finish() throws Exception {
        taskList.clear();
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        @NotNull String taskId = taskList.get(0).getId();
        @Nullable final String userId = taskList.get(0).getUser().getId();
        Assert.assertEquals(taskId, taskService.findOneById(userId, taskId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById("", taskId).getId());
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<Task> userTaskList = taskService.findAll(userUserId);
        Assert.assertEquals(2, userTaskList.size());
    }

    @SneakyThrows
    @Test
    public void testFindAllByProjectId() {
        @NotNull final String taskId = taskList.get(1).getId();
        Assert.assertEquals(0, taskService.findAllByProjectId(userAdminId, taskProjectId).size());
        Assert.assertEquals(1, taskService.findAllByProjectId(userUserId, taskProjectId).size());
    }

    @Test
    public void testRemoveAll() throws Exception {
        taskService.removeAll(userUserId);
        Assert.assertEquals(0, taskService.getSize(userUserId));
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        taskService.remove(userUserId, taskList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, taskService.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        taskService.removeOneById(userUserId, taskList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, taskService.getSize());
    }

    @SneakyThrows
    @Test
    public void testChangeTaskStatusById() {
        @NotNull final String taskId = taskList.get(1).getId();
        Assert.assertEquals(Status.NOT_STARTED.toString(), taskService.findOneById(userUserId, taskId).getStatus());
        taskService.changeTaskStatusById(userUserId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS.toString(), taskService.findOneById(userUserId, taskId).getStatus());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", taskId, Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, taskId, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userUserId, "", Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userUserId, null, Status.IN_PROGRESS));
    }

    @SneakyThrows
    @Test
    public void testUpdateById() {
        @NotNull final String taskId = taskList.get(1).getId();
        Assert.assertEquals("Task Name 2", taskService.findOneById(userUserId, taskId).getName());
        Assert.assertEquals("description test 2", taskService.findOneById(userUserId, taskId).getDescription());
        taskService.updateById(userUserId, taskId, "NewNameTask", "NewDescriptionTask");
        Assert.assertEquals("NewNameTask", taskService.findOneById(userUserId, taskId).getName());
        Assert.assertEquals("NewDescriptionTask", taskService.findOneById(userUserId, taskId).getDescription());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", taskId, "NewNameTask", "NewDescriptionTask"));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, taskId, "NewNameTask", "NewDescriptionTask"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userUserId, "", "NewNameTask", "NewDescriptionTask"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userUserId, null, "NewNameTask", "NewDescriptionTask"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userUserId, taskId, "", "NewDescriptionTask"));
    }

}
