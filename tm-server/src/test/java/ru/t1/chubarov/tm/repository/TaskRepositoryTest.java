package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.model.IProjectModelRepository;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.ProjectRepository;
import ru.t1.chubarov.tm.repository.model.TaskRepository;
import ru.t1.chubarov.tm.repository.model.UserRepository;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private final User userFirst = new User();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private final String taskProjectId = UUID.randomUUID().toString();

    @NotNull
    private final Project projectTask = new Project();

    @NotNull
    private final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final ITaskModelRepository taskRepository = new TaskRepository(entityManager);

    @NotNull
    private final IUserModelRepository repositoryUser = new UserRepository(entityManager);

    @NotNull
    private final IProjectModelRepository projectRepository = new ProjectRepository(entityManager);

    @SneakyThrows
    @Before
    public void initRepository() {
        entityManager.getTransaction().begin();
        userFirst.setId(userIdFirst);
        userFirst.setRole(Role.USUAL.toString());
        userFirst.setLocked(false);
        repositoryUser.add(userFirst);
        @NotNull final User userSecond = new User();
        userSecond.setId(userIdSecond);
        userSecond.setRole(Role.USUAL.toString());
        userSecond.setLocked(false);
        repositoryUser.add(userSecond);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        projectTask.setId(taskProjectId);
        projectTask.setName("project for Task");
        projectTask.setDescription("project description for Task.");
        projectTask.setUser(userFirst);
        projectRepository.add(projectTask);
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task_" + i);
            task.setProject(projectTask);
            task.setDescription("task description_" + i);
            if (i <= 5) task.setUser(userFirst);
            else task.setUser(userSecond);
            taskRepository.add(task);
            taskList.add(task);
        }
        entityManager.getTransaction().commit();
    }

    @SneakyThrows
    @After
    public void finish() {
        taskList.clear();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAll(userIdFirst);
            taskRepository.removeAll(userIdSecond);
            projectRepository.clear();
            repositoryUser.clear();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            throw e;
        }
        entityManager.close();
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        @NotNull final Task task = new Task();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setUser(userFirst);
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualtask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualtask);
        Assert.assertEquals(userIdFirst, actualtask.getUser().getId());
        Assert.assertEquals(name, actualtask.getName());
        Assert.assertEquals(description, actualtask.getDescription());
    }

    @SneakyThrows
    @Test
    public void testRemoveAllUser() {
        entityManager.getTransaction().begin();
        taskRepository.removeAll(userIdFirst);
        entityManager.getTransaction().commit();
        Assert.assertEquals(5, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindAllUser() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAllByUser(userIdFirst);
        Assert.assertEquals(5, actualTaskList.size());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test Task";
        @NotNull final Task task = new Task();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setUser(userFirst);
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualTask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        @NotNull final String taskId2 = actualTask.getId();
        Assert.assertNotNull(taskId2);
        Assert.assertEquals(taskId2, taskRepository.findOneById(taskId2).getId());
    }

    @SneakyThrows
    @Test
    public void testFindByProjectId() {
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(userIdFirst, taskProjectId);
        Assert.assertEquals(5, tasks.size());
        Assert.assertEquals(0, taskRepository.findAllByProjectId(userIdFirst, "fail_roject_id").size());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        entityManager.getTransaction().begin();
        taskRepository.remove(taskList.get(1));
        entityManager.getTransaction().commit();
        Assert.assertEquals(9, taskRepository.getSize());
        Assert.assertEquals(4, taskRepository.getSizeByUser(userIdFirst));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("Test Task");
        task.setUser(userFirst);
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(userIdFirst, "fail_test_id");
        entityManager.getTransaction().commit();
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final Task actualTask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(userIdFirst, actualTask.getId());
        entityManager.getTransaction().commit();
        Assert.assertEquals(numberOfTask - 1, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistById() {
        @NotNull final String name = "Test Task";
        @NotNull final Task task = new Task();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setDescription("Test Task Desc");
        task.setUser(userFirst);
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.existsById(userIdFirst, taskId));
        Assert.assertFalse(taskRepository.existsById(userIdFirst, "1111"));
        Assert.assertFalse(taskRepository.existsById("userId-123-4", taskId));
    }

}
